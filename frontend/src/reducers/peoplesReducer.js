import {
  RECEIVE_PEOPLES,
  REQUEST_PEOPLES,
  ERROR_PEOPLES,
  ADD_PEOPLE,
  FITLER_SEARCH_PERSON,
} from "../actions/requestPeoplesAction";

const initialState = {
  isLoading: false,
  entries: {},
  error: false,
};

export const peoplesReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_PEOPLES:
      return {
        ...state,
        isLoading: true,
      };
    case RECEIVE_PEOPLES:
      return {
        isLoading: false,
        entries: action.payload,
        error: false,
      };
    case ADD_PEOPLE:
      return {
        isLoading: false,
        entries: action.payload,
        error: false,
      };
    case ERROR_PEOPLES:
      return {
        isLoading: false,
        error: true,
      };
    case FITLER_SEARCH_PERSON:
      return {
        entries: action.payload,
        isLoading: false,
        error: false,
      };
    default:
      return state;
  }
};
