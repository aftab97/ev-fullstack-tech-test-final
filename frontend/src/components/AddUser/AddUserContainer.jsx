import React, { useState } from "react";

import { Button, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { addPeoplesAction } from "../../actions/requestPeoplesAction";
import { Box } from "@mui/system";
import { AddUser } from "./AddUser";

export const AddUserContainer = () => {
  const dispatch = useDispatch();
  const people = useSelector((state) => state.peoples.entries);

  const [person, setPerson] = useState({
    first_name: "",
    last_name: "",
    email: "",
    username: "",
  });

  const handleClick = () => {
    dispatch(addPeoplesAction(people, person));
  };

  const handleChange = (e) => {
    setPerson({ ...person, [e.target.name]: e.target.value });
  };

  return (
    <AddUser
      handleChange={handleChange}
      handleClick={handleClick}
      person={person}
      setPerson={setPerson}
    />
  );
};
