import { Button, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

export const AddUser = (props) => {
  return (
    <Box
      component="form"
      sx={{
        "& > :not(style)": { m: 1, width: "25ch" },
      }}
      noValidate
      autoComplete="off"
    >
      <TextField
        id="outlined-basic"
        label="First Name"
        variant="outlined"
        name="first_name"
        onChange={props.handleChange}
        value={props.person?.first_name}
      />
      <TextField
        id="outlined-basic"
        label="Last Name"
        variant="outlined"
        name="last_name"
        onChange={props.handleChange}
        value={props.person?.last_name}
      />
      <TextField
        id="outlined-basic"
        label="Username"
        variant="outlined"
        name="username"
        onChange={props.handleChange}
        value={props.person?.username}
      />
      <TextField
        id="outlined-basic"
        label="Email"
        variant="outlined"
        name="email"
        onChange={props.handleChange}
        value={props.person?.email}
      />
      <Button onClick={props.handleClick}>ADD USER</Button>
    </Box>
  );
};
