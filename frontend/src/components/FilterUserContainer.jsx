import { Button, TextField } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { filterPeopleAction } from "../actions/requestPeoplesAction";

import styled from "styled-components";

const Div = styled.div`
  display: flex;
  width: 40% !important;
  justify-content: left;
`;

export const FilterUserContainer = () => {
  const people = useSelector((state) => state.peoples.entries);
  const dispatch = useDispatch();

  const [filter, setFilter] = useState("");

  const handleClick = (e) => {
    dispatch(filterPeopleAction(people, filter));
  };

  return (
    <Box
      component="form"
      sx={{
        "& > :not(style)": { m: 1, width: "25ch" },
      }}
      noValidate
      autoComplete="off"
    >
      <Div>
        <TextField
          id="outlined-basic"
          label="Filter by (name / username / email)"
          variant="outlined"
          name="filter"
          onChange={(e) => {
            setFilter(e.target.value);
          }}
          value={filter}
        />
        <Button onClick={handleClick}>Filter</Button>
      </Div>
    </Box>
  );
};
