export const filterByValue = (array, string) => {
  return array.filter((o) =>
    Object.keys(o).some((k) =>
      o[k].toString().toLowerCase().includes(string.toLowerCase())
    )
  );
};
