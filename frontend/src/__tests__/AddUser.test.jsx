import React from "react";
import ReactDom from "react-dom";

import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import TestRenderer from "react-test-renderer";

import { TextField } from "@mui/material";
import { AddUser } from "../components/AddUser/AddUser";

it("renders without crashing", () => {});

// when first created creates a file in the test dir creating a SS of the button component and will
// check to see if any changes occur and if they do not match then it fails
it("matches snapshot 1", () => {
  const tree = TestRenderer.create(
    <TextField label="First Name"></TextField>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

it("matches snapshot 2", () => {
  const tree = TestRenderer.create(
    <TextField label="Last Name"></TextField>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

// snapshots are used when someone makes change to your code without permission so that way its a fail safe from stopping
// unexpected code being merged into the master branch

//** Test Cases  <AddUser />  Component */

const setup = () => {
  const utils = render(<AddUser />);
  const input = utils.getByLabelText("User");
  return {
    input,
    ...utils,
  };
};

describe("<AddUser /> PASS", () => {
  test("Should Render AddUser  Check Match Dom & Enter Value & Search  ", () => {
    render(<AddUser />);

    // if the data was static and not random this test would work ->
    // const { input } = setup();
    // fireEvent.change(input, { target: { value: 'jim jones' } });
    // expect(input.value).toBe('jim jones');

    expect(screen.getByLabelText("First Name")).toBeInTheDocument();
  });

  test("Should Render AddUser  Click  Search Button & Match Dom ", () => {
    render(<AddUser />);
    expect(screen.getByRole("button")).toBeInTheDocument();
    fireEvent.click(screen.getByRole("button"));
  });
});

describe("<AddUser /> PASS", () => {
  test("Should Render AddUser  Click  Search Button & Match Dom ", () => {
    render(<AddUser />);
    const submitButton = screen.queryByText("ADD USER");
    expect(submitButton).not.toEqual("ADD USER");
  });
});
