import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { PeopleTableContainer } from "../components/PeopleTableContainer";
import React from "react";

//** Test Cases  <PeopleTableContainer />  Component */

describe("<BasicTextFields /> PASS", () => {
  test("Should Render BasicTextFields Match Table & Dom Text ", () => {
    render(<PeopleTableContainer />);
    expect(screen.getByText("First Name").textContent).toEqual("First Name");
    expect(screen.getByText("Last Name").textContent).toEqual("Last Name");
    expect(screen.getByText("Username").textContent).toEqual("Username");
    expect(screen.getByText("Email").textContent).toEqual("Email");
  });
});

// describe("<BasicTextFields /> Fail", () => {
//   test("Should Render BasicTextFields Match Table & Dom Text ", () => {
//     render(<PeopleTableContainer />);
//     expect(screen.getByText("First Name").textContent).not.toEqual(
//       "First Name"
//     );
//     expect(screen.getByText("Last Name").textContent).not.toEqual("Last Name");
//     expect(screen.getByText("Username").textContent).not.toEqual("Username");
//     expect(screen.getByText("Email").textContent).not.toEqual("Email");
//   });
// });
