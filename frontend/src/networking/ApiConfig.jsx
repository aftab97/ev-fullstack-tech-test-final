import Config from "../config/Config";

export const ApiConfig = {
  base_path: Config.base_path,
};

export const ApiEndPointsConfig = {
  users: "/users",

  // lookup: "/lookup",
};
