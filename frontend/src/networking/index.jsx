import * as ApiConfig from "./ApiConfig";
import * as EndPoints from "./Endpoints";
import * as Interceptor from "./Interceptor";

export default {
  ApiConfig,
  EndPoints,
  Interceptor,
};
