import { ApiEndPointsConfig } from "./ApiConfig";

let endpoints = {
  getSearchResult: "",
};

class apiendPoints {
  getEndpoints = () => {
    endpoints.getSearchResult = ApiEndPointsConfig.users;
    return endpoints;
  };
}

const apiendPointsClass = new apiendPoints();

export default apiendPointsClass;
