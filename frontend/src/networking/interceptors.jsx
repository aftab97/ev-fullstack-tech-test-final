import axios from "axios";
import { ApiConfig } from "./ApiConfig";

// any additional settings to add to your api requests: (headers /tokens/ timeouts multiple API's)
let axiosInstance = axios.create({
  timeout: 10000,
  baseURL: ApiConfig.base_path,
  headers: {
    Accept: "application/json",
  },
});

export default axiosInstance;
