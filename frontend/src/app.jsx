import React, { useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import { requestPeoplesAction } from "./actions/requestPeoplesAction";
import { AddUserContainer } from "./components/AddUser/AddUserContainer";
import { FilterUserContainer } from "./components/FilterUserContainer";
import { PeopleTableContainer } from "./components/PeopleTableContainer";

const App = () => {
  const dispatch = useDispatch();
  const results = useSelector((state) => state.peoples);

  // load up people on app start
  useEffect(() => {
    dispatch(requestPeoplesAction());
  }, []);

  return (
    <div>
      <AddUserContainer />
      <FilterUserContainer />
      {results && <PeopleTableContainer data={results} />}
    </div>
  );
};

export default App;
