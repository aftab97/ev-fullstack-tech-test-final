import Axios from "axios";
import { useSelector } from "react-redux";
import { ApiEndPointsConfig } from "../networking/ApiConfig";
import axiosInstance from "../networking/interceptors";
import { filterByValue } from "../utils/utils";

export const ADD_PEOPLE = "ADD_PEOPLE";
export const REQUEST_PEOPLES = "REQUEST_PEOPLES";
export const RECEIVE_PEOPLES = "RECEIVE_PEOPLES";
export const ERROR_PEOPLES = "ERROR_PEOPLES";
export const FITLER_SEARCH_PERSON = "FILTER_SEARCH_PERSON";
export const FILTER_NO_PERSON = "FILTER_NO_PERSON";

const requestUsers = () => ({
  type: REQUEST_PEOPLES,
});

const receiveUsers = (peoples) => ({
  type: RECEIVE_PEOPLES,
  payload: peoples,
});

const errorPeoples = () => ({
  type: ERROR_PEOPLES,
});

const addPeople = (peoples) => ({
  type: ADD_PEOPLE,
  payload: peoples,
});

const filterPeople = (peoples) => ({
  type: ADD_PEOPLE,
  payload: peoples,
});

export const requestPeoplesAction = () => {
  return async (dispatch) => {
    // <---- Action for if there was a API (deals with multiple APIS/ interceptors/ headers/ different endpoints) ------>
    let url = `${ApiEndPointsConfig.users}/random_user?size=30`;

    dispatch(requestUsers());
    axiosInstance
      .get(url)
      .then((response) => {
        dispatch(receiveUsers(response.data));
      })
      .catch((error) => {
        dispatch(errorPeoples(error));
      });
  };
};

export const addPeoplesAction = (
  currentPeoples,
  { first_name, last_name, email, username }
) => {
  return async (dispatch) => {
    console.log(currentPeoples);

    const data = [
      {
        first_name,
        last_name,
        username,
        email,
      },
      ...currentPeoples,
    ];

    dispatch(addPeople(data));
  };
};

export const filterPeopleAction = (currentPeoples, filter) => {
  return async (dispatch) => {
    const data = filterByValue(currentPeoples, filter);

    dispatch(filterPeople(data));
  };
};
